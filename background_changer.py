import praw, os, random, requests

# This function downloads the images from reddit
# The if/elif statement there is to ensure that only images are downloaded

def downloadimage(submission, fileName):
    """
    This function downloads the images from reddit. The if/elif statement is there
    to ensure that only images are downloaded.
    """
    response = requests.get(submission.url, stream=True)
    with open(fileName, 'wb') as fo:
        for chunk in response.iter_content(4096):
            fo.write(chunk)
    command = "mv /home/kailash/Programming/Python/BackgroundChanger/" + fileName + " /home/kailash/Pictures/Wallpapers"
    os.system(command)

def changebackground():
    """
    This function randomly chooses a picture from the wallpaper directory, and issues the
    terminal command to change the background.
    """
    random_background = random.choice(os.listdir("/home/kailash/Pictures/Wallpapers"))
    new_background = "/home/kailash/Pictures/Wallpapers/" + random_background
    cmd = "gsettings set org.gnome.desktop.background picture-uri 'file:%s'" % (new_background)
    os.system(cmd)

def main():
    user_agent = praw.Reddit("/u/giantglass's image downloader")
    for submission in user_agent.get_subreddit('earthporn').get_hot(limit = 3):
        fileName = "%s" % (submission.id)
        if "jpeg" in submission.url or "jpg" in submission.url and fileName not in os.listdir("/home/kailash/Pictures/Wallpapers"): # Downloads all directly linked images, regardless of websites
            downloadimage(submission, fileName)
        elif "imgur.com" in submission.url and "i.imgur.com" not in submission.url and fileName not in os.listdir("/home/kailash/Pictures/Wallpapers"): # Downloads images uploaded to imgur
            fileName = "%s" % (submission.id)
            downloadimage(submission, fileName)
    
    # Change the background

    changebackground()

if __name__ == "__main__":
    main()
